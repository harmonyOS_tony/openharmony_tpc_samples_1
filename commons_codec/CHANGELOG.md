## 1.0.0
1.支持二进制编码器Base64编解码、Base32编解码、二进制、十六进制。

2.支持摘要编码器SHA256、SHA224、SHA1、MD5、MD2。

3.支持语言编码器CaverPhone、Soundex、Metaphone。

4.支持url编码urlencode。
