
## 测试说明
1. pc设备和板子链接同一网络(交换机或路由器或无线网)  
2. 设备间能互相ping通，RK3568设备 查看ip命令：
    ```
    hdc shell ifconfig
    ````
   备注：eth0下inet addr 即为客户端RK3568的ip  

3. pc设备下 共享某一文件夹 设置用户  可读可写  
4. 板子链接pc的ip和share（如：\\\\192.168.112.1\\test_smbj）； 访问共享路径下的文件（test.txt)  

备注：1.如需手动设置RK3568 IP 参考RK3568手动设置IP.bat（hdc_std/hdc）
     2.分享的文件或文件夹不存在进行读写，会主动抛出异常，调用出回调该异常；
     ```
     //message.ets
     var error = new Error(MsErref.getErrorMessage(err));
     error.code = err.code;
     cb && cb(error);
     ```
   