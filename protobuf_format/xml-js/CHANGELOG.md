### 1.0.1

- fix:修改README.OpenSource文件中依赖组件的License File路径信息

### 1.0.1-rc.1

- 适配ArkTs语法

### 1.0.1-rc.0

- 修复不兼容API9问题

### 1.0.0
1.基于xml-js@1.6.11版本进行移植。

2.支持xml与json互转。

3.支持xml与js互转。

