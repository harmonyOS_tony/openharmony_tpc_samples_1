/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import leven1 from 'leven';

export default function levenTest() {
  describe('levenTest', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    // it('assertContain',0, function () {
    //   // Defines a test case. This API supports three parameters: test case name, filter parameter, and test case function.
    //   hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
    //   hilog.info(0x0000, 'testTag', '%{public}s', 'it begin');
    //   let a = 'abc'
    //   let b = 'b'
    //   // Defines a variety of assertion methods, which are used to declare expected boolean conditions.
    //   expect(a).assertContain(b)
    //   expect(a).assertEqual(a)
    // })

    let count = 0;
    let test =  (name:string, func:Function)=>{
      name = name.replace(new RegExp("/[ /d]/g"), '');
      name = name.replace(new RegExp("/-/g"),"");
      it(name,count++,func)
    }

    let equal =  (src:number, dst:number, tag?:string) =>{
      console.log('tag:' + tag + "  src=" + src + " dst=" + dst);
      expect(src).assertEqual(dst)
    }


    test('leven', () => {
      equal(leven1('a', 'b'), 1);
      equal(leven1('ab', 'ac'), 1);
      equal(leven1('ac', 'bc'), 1);
      equal(leven1('abc', 'axc'), 1);
      equal(leven1('kitten', 'sitting'), 3);
      equal(leven1('xabxcdxxefxgx', '1ab2cd34ef5g6'), 6);
      equal(leven1('cat', 'cow'), 2);
      equal(leven1('xabxcdxxefxgx', 'abcdefg'), 6);
      equal(leven1('javawasneat', 'scalaisgreat'), 7);
      equal(leven1('example', 'samples'), 3);
      equal(leven1('sturgeon', 'urgently'), 6);
      equal(leven1('levenshtein', 'frankenstein'), 6);
      equal(leven1('distance', 'difference'), 5);
      equal(leven1('因為我是中國人所以我會說中文', '因為我是英國人所以我會說英文'), 2);
    })

  })
}
