## v2.0.0

- 包管理工具由npm切换为ohpm
- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)

## v1.0.1

- 适配3.1.0.200 IDE

## v1.0.0

- 实现功能

  作为与服务端之间建立链接的客户端库，使用c代码在ohos平台编译成.so文件，对外暴露connect、on、open、off、close、emit等接口。
