[
    {
        "Name": "js-tokens",
        "License": "MIT License",
        "License File": "https://github.com/lydell/js-tokens/blob/main/LICENSE",
        "Version Number": "v5.0.0",
        "Owner":"lydell",
        "Upstream URL": "https://github.com/lydell/js-tokens",
        "Description": "Tiny JavaScript tokenizer."
    }
]